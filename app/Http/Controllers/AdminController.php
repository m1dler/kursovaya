<?php

namespace App\Http\Controllers;

use App\Models\Attraction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.index', ['data' => (new \App\Models\Attraction)->index()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.add', ['attractions' => (new \App\Models\Attraction_list())->allData()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $add = new Attraction;
        $add->guest_name = $request->input('guest_name');
        $add->attraction_type = $request->input('attraction_type');
        $add->data = $request->input('date');
        $add->save();
        return redirect()->route('admin.index');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.edit', [
            'result' => (new \App\Models\Attraction)->IDshow($id),
            'attractions' => (new \App\Models\Attraction_list())->allData()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::table('attraction')
            ->where('attraction.id', $id)
            ->update([
                'guest_name' => $request->input('guest_name'),
                'attraction_type' => $request->input('attraction_type'),
                'data' => $request->input('date')]);
        return redirect()->route('admin.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('attraction')->where('id', '=', $id)->delete();
        return redirect()->route('admin.index');
    }
}
