<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MainController extends Controller
{
    protected function show()
    {
        return view('guest.index', ['data' => (new \App\Models\Attraction)->index()]);
    }


    public function find(Request $req)
    {
        $id = $req->input('attraction_id');
        return view('guest.find', ['data' => (new \App\Models\Attraction)->show($id)]);
    }
}
