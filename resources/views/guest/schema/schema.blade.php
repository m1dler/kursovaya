<!doctype html>
<html lang="ua">
<head>
    <title>@yield('title-block')</title>
    <meta charset="utf-8"/>
    <link rel="stylesheet" href="../../../css/app.css">
</head>
<body>
<center>
<p>
<h2>Парк розваг</h2>
@yield('content')
@if(isset($data))
    <table>
        <thead>
        <tr>
            <th>Ім'я відвідувача</th>
            <th>Атракціон</th>
            <th>Ціна</th>
            <th>Дата</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($data as $row)
            <tr>
                <td>{{$row->guest_name}}</td>
                <td>{{ $row->attraction}}</td>
                <td> {{$row->price}} </td>
                <td> {{$row->data}} </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endif
</center>
</body>
</html>
