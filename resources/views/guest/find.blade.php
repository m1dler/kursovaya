@extends('guest.schema.schema')

@section('title-block','Find')
<?php
$model_attraction = new App\Models\Attraction_list();
$att_list = $model_attraction->allData();
?>
@section('content')
    <form action="{{route('find-form')}}" method="post">
        @csrf
        <p>
        <label for="id">Виберіть атракціон:</label>
        <select name="attraction_id" id="id">
        @foreach($att_list as $attraction)
            <option value="{{ $attraction->attraction_id }}"
                    @if (isset($_POST['attraction_id']) && $_POST['attraction_id'] == $attraction->attraction_name)
                    selected
                @endif
            >
                {{ $attraction->attraction_name }}
            </option>
        @endforeach
        </select>
            <button type="submit">Пошук</button>
        </p>
    </form>
    <p>
        <a href="{{route("index")}}">Назад</a>
    </p>
@endsection
