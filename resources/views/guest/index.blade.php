@extends('guest.schema.schema')

@section('title-block','Main')

@section('content')
    <p>
        <a href="{{route('find')}}">Пошук</a>
        |
        <a href="{{ route('login') }}">{{ __('Login') }}</a>
    </p>
@endsection
