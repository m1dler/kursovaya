@extends('admin.schema.schema')

@section('title-block','Admin')

@section('content')
    <p>
        <a href="{{route('admin.index')}}">Головна</a>
        |
        <a href="{{route('admin.create')}}">Додати</a>
        |
        <a href="{{ route('logout') }}"
           onclick="event.preventDefault();
        document.getElementById('logout-form').submit();">
            {{ __('Logout') }}
        </a>
    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
        @csrf
    </form>
    </p>
@endsection
