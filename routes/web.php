<?php

use Illuminate\Support\Facades\Route;

Route::get('/', 'App\Http\Controllers\MainController@show')->name('index');

Route::get('/find', function () {
    return view('guest.find');
})->name('find');

Route::post('/find/submit', 'App\Http\Controllers\MainController@find')->name('find-form');

Route::middleware(['auth'])->group(function () {
    Route::resource('admin', 'App\Http\Controllers\AdminController', ['parameters' => [
        'admin' => 'id'
    ]]);
});

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Auth::routes([
    'reset' => false,
    'confirm' => false,
    'verify' => false,
    'register' => false,
]);
